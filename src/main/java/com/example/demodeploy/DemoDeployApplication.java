package com.example.demodeploy;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.data.redis.connection.jedis.JedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.repository.configuration.EnableRedisRepositories;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@RestController
@EnableRedisRepositories
public class DemoDeployApplication {

	public static void main(String[] args) {
		SpringApplication.run(DemoDeployApplication.class, args);
	}

	@GetMapping("/")
	public String home(){
		return "Welcome Home";
	}


}
